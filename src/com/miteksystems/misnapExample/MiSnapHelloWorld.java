package com.miteksystems.misnapExample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.miteksystems.misnap.MiSnap;
import com.miteksystems.misnap.MiSnapAPI;

/*
 * entry activity for MiSnapHelloWorld
 * This exercises the MiSnap API by calling MiSnap for a result with
 * mostly default parameters and awaiting the callback for onActivityResult
 */
public class MiSnapHelloWorld extends Activity {
	static final int AUTO_INTERVAL_MILLIS = 1000;
	
	Handler mHandler;
	boolean mStarted = false;
	
    /** Called when the activity is first created. */
    @SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (android.os.Build.VERSION.SDK_INT > 15) {
        	  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        	  StrictMode.setThreadPolicy(policy);
        	 }
        try {Thread.sleep(5000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mHandler = new Handler();
        mHandler.post(mStart);
    }
    
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
        mStarted = false;
        if (requestCode == MiSnapAPI.RESULT_PICTURE_CODE && resultCode == Activity.RESULT_OK && data != null)
		{
        	// Image returned successfully
        	byte[] image = data.getByteArrayExtra(MiSnapAPI.RESULT_PICTURE_DATA);
        	sendRequest(image); 
			mHandler.removeCallbacks(mStart);
            mHandler.postDelayed(mStart, AUTO_INTERVAL_MILLIS);
		}
		else if(requestCode == MiSnapAPI.RESULT_PICTURE_CODE && resultCode == Activity.RESULT_OK && data == null) {
        	// Image canceled, stop
        	Toast.makeText(this, "MiSnap canceled", Toast.LENGTH_LONG).show();
		}
		else if (resultCode == Activity.RESULT_CANCELED) {
			// Camera not working or not available, stop
        	Toast.makeText(this, "Camera not working or canceled", Toast.LENGTH_LONG).show();
		}
	}
	
	private void sendRequest(byte[] image) {
		/*StringBuilder stringBuilder = new StringBuilder ();
		Base64 encoder = new Base64();
		
		stringBuilder.append(String.format("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"" +
	            " xmlns:mit=\"http://www.miteksystems.com/\">" +
	            "<soapenv:Header/>" +
	            "<soapenv:Body>" +
			    "<mit:InsertPhoneTransaction>" + 
		        "<mit:userName>ncholaky@ucsd.edu</mit:userName>" +
		        "<mit:password>8CCiUeQk7KGD</mit:password>" +
		         "<mit:phoneKey>MitekTest</mit:phoneKey>" +
		         "<mit:orgName>UCSD-Hack</mit:orgName>" +
		         "<!--base64 encoded image string:-->" +
		         "<mit:base64Image>"+encoder.encodeBase64(image)+"</mit:base64Image>" +
		         "<mit:compressionLevel>50</mit:compressionLevel>" +
		         "<mit:documentIdentifier>Driver_License_WI</mit:documentIdentifier>" +
		         "<mit:documentHints></mit:documentHints>" +
		         "<mit:dataReturnLevel>30</mit:dataReturnLevel>" +
		         "<mit:returnImageType>0</mit:returnImageType>" +
		         "<mit:rotateImage>0</mit:rotateImage>" +
		         "<mit:note>Mitek Test</mit:note>" +
		      "</mit:InsertPhoneTransaction>" +
		   "</soapenv:Body>" +
		"</soapenv:Envelope>" +	            
	            "</soapenv:Body>" +
	            "</soapenv:Envelope>"));

	    String xml = stringBuilder.toString();
	    HttpPost httpPost = new HttpPost("https://mip03.ddc.mitekmobile.com/MobileImagingPlatformWebServices/ImagingPhoneService.asmx");
	    StringEntity entity;
	    String response_string = null;

	    try {
	    	
	        
	        entity = new StringEntity(xml, HTTP.UTF_8);
	        System.out.println("POINT: 1");
	        httpPost.setHeader("Content-Type","text/xml;charset=UTF-8");
	        System.out.println("POINT: 2");
	        httpPost.setEntity(entity);
	        System.out.println("POINT: 3");
	        HttpClient client = new DefaultHttpClient();
	        System.out.println("POINT: 4");
	        HttpResponse response = client.execute(httpPost);
	        System.out.println("POINT: 5");
	        response_string = EntityUtils.toString(response.getEntity());
	        System.out.println("POINT: 1");
	        Log.d("request", response_string);
	        System.out.println(response_string);
	        //edit().putString("response", response_string).commit();

	    } 

	    catch (Exception e) {
	        e.printStackTrace();
	    }*/

String soap="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mit=\"http://www.miteksystems.com/\">    <soapenv:Header/>  <soapenv:Body>       <mit:InsertPhoneTransaction>  <mit:userName>ncholaky@ucsd.edu</mit:userName>  <mit:password>8CCiUeQk7KGD</mit:password>  <mit:phoneKey>MitekTest</mit:phoneKey>  <mit:orgName>UCSD-Hack</mit:orgName>          <mit:base64Image>";
        	String result=Base64.encodeToString(image, Base64.DEFAULT);
			
        	String soap2="</mit:base64Image>  <mit:compressionLevel>50</mit:compressionLevel>  <mit:documentIdentifier>CROP_IQA_OCR</mit:documentIdentifier>  <mit:documentHints></mit:documentHints>  <mit:dataReturnLevel>30</mit:dataReturnLevel>  <mit:returnImageType>0</mit:returnImageType>  <mit:rotateImage>0</mit:rotateImage>          <mit:note>Mitek Test</mit:note>       </mit:InsertPhoneTransaction>  </soapenv:Body> </soapenv:Envelope>";
        	
        	HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new
HttpPost("https://mip03.ddc.mitekmobile.com/MobileImagingPlatformWebServices/ImagingPhoneService.asmx");
            httppost.setHeader("Content-Type", "text/xml");
            // Add your data
            try {
            	StringBuilder builder = new StringBuilder();
            	builder.append(soap).append(result).append(soap2);
				httppost.setEntity(new StringEntity(builder.toString()));
	            // Execute HTTP Post Request
	            HttpResponse response = httpclient.execute(httppost);
	            InputStream is = response.getEntity().getContent();
	            BufferedReader ir = new BufferedReader(new
	            		InputStreamReader(is));

	            String receivedResponse = "";
	            String inputLine;
	            while ((inputLine = ir.readLine()) != null) {
	                Log.d("BEH",inputLine);
	                receivedResponse+=inputLine;
	            }
	            Log.i("test",receivedResponse);
	           String xml = "<hello><test>1.2</test><test2>123</test2></hello>";
	            JSONObject json = null; 
	            json = XML.toJSONObject(receivedResponse);
	            //JSONObject json = new JSONObject(receivedResponse);
	            //JSONArray jArray = jObject.getJSONArray("myArray");
	            //json.toJSONArray(receivedResponse);
	           // XMLSerializer xmlSerializer = new XMLSerializer();  
	            //JSON json = xmlSerializer.read(receivedResponse); 
	            //JSON json = xmlSerializer.read(xml); 
	            
	            HttpClient client = new DefaultHttpClient();
	            HttpPost post = new HttpPost("http://128.54.17.243:8080/ocr");
	            post.setHeader("Content-Type", "application/json");
	            Log.i("post",json.toString());
	            
	            String s = (String) json.getJSONObject("soap:Envelope").getJSONObject("soap:Body").getJSONObject("InsertPhoneTransactionResponse").getJSONObject("InsertPhoneTransactionResult").getJSONObject("Transaction").get("ExtractedRawOCR");
	           StringBuilder builder1 = new StringBuilder();
	           s = new String(Base64.encode(s.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP));
	           	           builder1.append("{ \"extract\" : \"").append(s).append("\"}");//.replaceAll("\n", "+")
	           Log.i("builder", builder1.toString());
	            
	            post.setEntity(new StringEntity(builder1.toString()));  
	            System.out.print(json.toString());
	            HttpResponse resp = client.execute(post);
	            HttpEntity ent = resp.getEntity();
	           
	            
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
	}

	// Start MiSnap
	Runnable mStart = new Runnable() {
		public void run() {
			mStarted = true;
			JSONObject jjs = null;
			try {
				jjs = new JSONObject();

				/////////////////  Override defaults
				//System.out.print("THIS IS: " + jjs.toString());
				jjs.put(MiSnapAPI.Name, "DocumentIdentifier");	// set job file
				
				jjs.put(MiSnapAPI.RequiredCompressionLevel, "30");
				//jjs.put(MiSnapAPI.CameraViewfinderMinFill, "400");	// For DLs
				jjs.put(MiSnapAPI.CameraViewfinderMinFill, "300");	// For ACH, REMITTANCE
				jjs.put(MiSnapAPI.CameraTimeoutInSeconds, "15");
				//jjs.put(MiSnapAPI.CameraAutoCaptureProcess, "2");	// Test for old MIP parameter compliance
				jjs.put(MiSnapAPI.CameraBrightness, "400");
				jjs.put(MiSnapAPI.CameraGForce, "2");
				jjs.put(MiSnapAPI.CameraSharpness, "400");
				//jjs.put(MiSnapAPI.CameraFlash, "1"); // off=0, auto=1, on=2, torch=3
				//jjs.put(MiSnapAPI.DeviceID, "whatever");
				
				jjs.put(MiSnapAPI.CameraVideoAutoCaptureProcess, "0");
				jjs.put(MiSnapAPI.CameraViewfinderBoundingBox, "1"); // 1=MiSnap, 0=Manual
				
				// All other non-default parameters set into jjs here
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			Intent i = new Intent(MiSnapHelloWorld.this, MiSnap.class);
			i.putExtra(MiSnapAPI.JOB_SETTINGS, jjs.toString());
			startActivityForResult(i, MiSnapAPI.RESULT_PICTURE_CODE);
		}
	};
	
	public void onSnapIt(View v) {
		if(!mStarted) {
			mHandler.removeCallbacks(mStart);
			mHandler.post(mStart);
		}
	}

    @Override
    public void onBackPressed() {
    	mHandler.removeCallbacks(mStart);
    	finish();
    }
    
}